﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEnable : MonoBehaviour
{
    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            audioSource.enabled = true;
        }
    }



}
