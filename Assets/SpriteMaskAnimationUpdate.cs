﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMaskAnimationUpdate : MonoBehaviour
{

    SpriteRenderer sprite;
    SpriteMask mask;
    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        mask = GetComponent<SpriteMask>();
    }

    private void FixedUpdate()
    {
        mask.sprite = sprite.sprite;
    }
}
