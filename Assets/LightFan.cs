﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFan : MonoBehaviour
{
    public float rotationSpeed = 1f;

    float rotation = 0f;

    void Update()
    {
        rotation += rotationSpeed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0f, 0f, rotation);


    }
}
