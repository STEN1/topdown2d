﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceBolt : MonoBehaviour
{
    Rigidbody2D rb;
    SpawnExplosion spawnExplosion;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        spawnExplosion = GetComponentInChildren<SpawnExplosion>();

        Vector2 force = new Vector2(10, 0);
        rb.AddRelativeForce(force, ForceMode2D.Impulse);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player") && !collision.CompareTag("MainCamera"))
        {
            spawnExplosion.Explosion();
            Destroy(gameObject);
        }
    }
}