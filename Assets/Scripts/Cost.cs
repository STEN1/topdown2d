﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cost : MonoBehaviour
{
    public int manaCost;
    public int staminaCost;
}
