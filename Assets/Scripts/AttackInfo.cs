﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackInfo : MonoBehaviour
{
    public int dmg = 1;
    public float pushForce = 5f;
    public string dmgType = "Magic";
    public float attackSpeed  = 1f;
    public int manaCost = 1;
    public int staminaCost = 1;

    public bool disableMovementWhileAttacking = true;
    public bool hasTravelTime = false;
}
