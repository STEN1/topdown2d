﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ManaBar : MonoBehaviour
{
    public Slider slider;

    public void SetManaUI(int mana, int maxMana)
    {
        slider.maxValue = maxMana;
        slider.value = mana;
    }

    public void SetManaUI(int mana)
    {
        slider.value = mana;
    }
}
