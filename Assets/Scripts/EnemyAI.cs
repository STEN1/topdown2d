﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyAI : MonoBehaviour
{

    public float detectionRadius = 10f;

    public float speed = 200f;
    public float nextWaypointDistance = 3f;

    bool dead = false;

    Vector2 directionPath = new Vector2(1f, 0f);
    Vector2 directionTarget;
    float rotationDirection = 0f;
    float distanceToTarget = 100f;

    Path path;
    int currentWaypoint = 0;

    Seeker seeker;
    Rigidbody2D rb;
    Animator animator;
    Unit unit;

    Transform target;

    #region Start FixedUpdate
    void Start()
    {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponentInChildren<Animator>();
        unit = GetComponent<Unit>();
        target = FindObjectOfType<Character>().transform;

        InvokeRepeating("UpdatePath", 0f, 0.5f);
        
    }



    void FixedUpdate()
    {
        if (unit.health <= 0)
        {
            dead = true;
            animator.Play("death");
        }

        // movement animation
        rotationDirection = Mathf.Atan2(rb.velocity.y, rb.velocity.x) * Mathf.Rad2Deg;
        if (rotationDirection < 0) rotationDirection += 360f;

        if (directionPath != Vector2.zero && !dead)
        {
            if (rotationDirection > 22.5f && rotationDirection <= 67.5) animator.Play("upright");
            else if (rotationDirection > 67.5 && rotationDirection <= 112.5) animator.Play("up");
            else if (rotationDirection > 112.5f && rotationDirection <= 157.5) animator.Play("upleft");
            else if (rotationDirection > 157.5f && rotationDirection <= 202.5) animator.Play("left");
            else if (rotationDirection > 202.5f && rotationDirection <= 247.5) animator.Play("downleft");
            else if (rotationDirection > 247.5f && rotationDirection <= 292.5) animator.Play("down");
            else if (rotationDirection > 292.5f && rotationDirection <= 337.5) animator.Play("downright");
            else animator.Play("right");
        }



        #region Pathfinding

        if (target == null)
            target = transform;

        if (path == null)
            return;

        if (currentWaypoint >= path.vectorPath.Count)
        {
            //reachedEndOfPath

            directionTarget = ((Vector2)target.position - rb.position).normalized;
            Vector2 directForce = directionTarget * speed * 2f * Time.fixedDeltaTime;
            rb.AddForce(directForce);

            return;
        }

        directionPath = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
        Vector2 force = directionPath * speed * Time.fixedDeltaTime;


        rb.AddForce(force);

        float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]); 

        if (distance < nextWaypointDistance)
        {
            currentWaypoint++;
        }
        #endregion
    }
    #endregion

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
    }

    #region Pathfinding
    void UpdatePath()
    {
        distanceToTarget = Vector2.Distance(rb.position, target.position);
        

        if (distanceToTarget < detectionRadius || unit.agro)
        {
            if (seeker.IsDone())
            {
                seeker.StartPath(rb.position, target.position, OnPathComplete);
                unit.agro = true;
            }
        }

    }

    void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }
    #endregion




}
