﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    GameObject character;

    [SerializeField] float followSpeed = 10f;

    [SerializeField] bool smoothFollow = true;


    Vector2 targetPos;



    private void MoveToTarget()
    {
        if (smoothFollow)
        {
            // Move camera between players
            if (character == null)
                return;
            targetPos = character.transform.position;
            transform.position = Vector2.Lerp(transform.position, targetPos, followSpeed * Time.fixedDeltaTime);
            transform.position = new Vector3(transform.position.x, transform.position.y, -10f);
            /*
            // Re-size camera.ortographicSize based on distance between players
            playerToPlayer = Player1.position - Player2.position;
            distancePlayerToPlayer = playerToPlayer.magnitude;
            cam.orthographicSize = (distancePlayerToPlayer + 2f) * 0.5f;
            if (cam.orthographicSize < 5f)
                cam.orthographicSize = 5f;
            */
        }
        else
        {
            if (character == null)
                return;
            targetPos = character.transform.position;
            transform.position = targetPos;
            transform.position = new Vector3(transform.position.x, transform.position.y, -10f);
        }
    }

    private void Start()
    {
        character = GameObject.FindGameObjectWithTag("Player");
        transform.position = character.transform.position;
    }

    private void FixedUpdate()
    {
        MoveToTarget();
    }


}
