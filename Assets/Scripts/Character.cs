﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{

    Rigidbody2D rb;
    Animator animator;
    AudioSource audioSource;
    Unit unit;


    StaminaBar staminaBar;
    ManaBar manaBar;
    HealthBar healthBar;

    //Movement
    [SerializeField] float moveSpeed = 10f;
    bool MoveDirZero = false;
    [SerializeField] string up = "w";
    [SerializeField] string down = "s";
    [SerializeField] string left = "a";
    [SerializeField] string right = "d";
    [HideInInspector] public Vector3 moveDir;
    [HideInInspector] public Vector3 lastMoveDir = new Vector3(1f, 0f);
    [HideInInspector] public Vector3 position;
    public float rotationMoveDir;
    public float rotationLastMoveDir;


    //Rolling
    [SerializeField] float rollForce = 10f;
    [SerializeField] string rollKey = "space";
    Vector3 rollDir;
    float rotationRollDir = 0f;
    bool isRolling = false;
    bool isRollKeyDown = false;

    //Attack
    bool isAttacking = false;
    bool isMeleeAttacking = false;
    bool isMeleeKeyDown = false;
    public float attackRotation = 0f;

    bool isRangedKeyDown = false;
    bool isRangedAttacking = false;

    //Mouse
    public Vector3 worldPositionMouse;
    public Vector3 distanceToMouse;
    public float rotationPlayerToMouse;
    Crosshair crosshair;

    //resources
    AudioClip step;
    AudioClip roll;
    public GameObject meleeAttack;
    public GameObject rangedAttack;




    #region Start Update FixedUpdate
    private void Start()
    {
        step = Resources.Load("Audio/Character_Step", typeof(AudioClip)) as AudioClip;
        roll = Resources.Load("Audio/Character_Roll", typeof(AudioClip)) as AudioClip;
        crosshair = GetComponentInChildren<Crosshair>();

        audioSource = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        unit = GetComponent<Unit>();
        staminaBar = FindObjectOfType<StaminaBar>();
        manaBar = FindObjectOfType<ManaBar>();
        healthBar = FindObjectOfType<HealthBar>();

        healthBar.SetHealthUI(unit.health, unit.maxHealth);
        InvokeRepeating("Health", unit.recoveryTimeHealth, unit.recoveryTimeHealth);

        manaBar.SetManaUI(unit.stamina, unit.maxMana);
        InvokeRepeating("Mana", unit.recoveryTimeMana, unit.recoveryTimeMana);

        staminaBar.SetStaminaUI(unit.stamina, unit.maxStamina);
        InvokeRepeating("Stamina", unit.recoveryTimeStamina, unit.recoveryTimeStamina);

    }
    private void Update()
    {
        if (unit.health < 1)
            Death();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseScreen pauseScreen = FindObjectOfType<Canvas>().transform.GetChild(2).GetComponent<PauseScreen>();
            if (pauseScreen != null)
                pauseScreen.Pause();
        }


        moveDir = moveDirFromInput();
        if (moveDir != Vector3.zero) lastMoveDir = moveDir;
        if (Input.GetKey(rollKey)) isRollKeyDown = true;
        if (Input.GetKey(KeyCode.Mouse0)) isMeleeKeyDown = true;
        if (Input.GetKey(KeyCode.Mouse1)) isRangedKeyDown = true;

        //lastMoveDir should never be zero
        if (lastMoveDir == Vector3.zero) Debug.Log("lastMoveDir = zero");
    }

    private void FixedUpdate()
    {
        SetMouseWorldPossitionAndRotation();
        RollMove();
        MeleeAttack();
        RangedAttack();
        Animation();
        position = transform.position;
        if (!isRolling)
            rb.AddForce(moveDir * moveSpeed * Time.fixedDeltaTime, ForceMode2D.Force);
        if (isRolling)
            rb.AddForce(rollDir * moveSpeed * Time.fixedDeltaTime, ForceMode2D.Force);
    }
    #endregion

    void Death()
    {
        FindObjectOfType<Canvas>().transform.GetChild(1).gameObject.SetActive(true);
        Destroy(gameObject);
    }

    #region collision
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemy") || collision.collider.CompareTag("WorldDamage"))
        {
            AttackInfo attackInfo = collision.collider.GetComponentInParent<AttackInfo>();

            Vector2 pushDir = (transform.position - attackInfo.transform.position).normalized;

            rb.AddForce(pushDir * attackInfo.pushForce, ForceMode2D.Impulse);

            unit.health -= attackInfo.dmg;
            healthBar.SetHealthUI(unit.health);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy") || collision.CompareTag("WorldDamage"))
        {
            AttackInfo attackInfo = collision.GetComponentInParent<AttackInfo>();

            Vector2 pushDir = (transform.position - attackInfo.transform.position).normalized;

            rb.AddForce(pushDir * attackInfo.pushForce, ForceMode2D.Impulse);

            unit.health -= attackInfo.dmg;
            healthBar.SetHealthUI(unit.health);
        }
    }

    #endregion

    #region Invoking methods
    private void Stamina()
    {
        if (unit.stamina < unit.maxStamina)
        {
            unit.stamina += 1;
            staminaBar.SetStaminaUI(unit.stamina);
        }
    }

    private void Mana()
    {
        if (unit.mana < unit.maxMana)
        {
            unit.mana += 1;
            manaBar.SetManaUI(unit.mana);
        }
    }

    private void Health()
    {
        if (unit.health < unit.maxHealth)
        {
            unit.health += 1;
            healthBar.SetHealthUI(unit.health);
        }
    }

    #endregion

    #region Skills

    void RollMove()
    {
        if (isRollKeyDown && !isAttacking && !isRolling && unit.stamina > 1)
        {
            isRolling = true;
            rb.AddForce(lastMoveDir * rollForce, ForceMode2D.Impulse);
            rollDir = lastMoveDir;

            unit.stamina -= 2;
            staminaBar.SetStaminaUI(unit.stamina);

            CancelInvoke("Stamina");
            InvokeRepeating("Stamina", unit.recoveryTimeStamina, unit.recoveryTimeStamina);
        }
        isRollKeyDown = false;
    }





    void MeleeAttack()
    {
        if (isMeleeKeyDown && !isAttacking && !isRolling && unit.stamina >= meleeAttack.GetComponent<AttackInfo>().staminaCost)
        {
            if(meleeAttack.GetComponent<AttackInfo>().disableMovementWhileAttacking)
                MoveDirZero = true;

            isAttacking = true;
            isMeleeAttacking = true;
            attackRotation = rotationPlayerToMouse;

            unit.stamina -= meleeAttack.GetComponent<AttackInfo>().staminaCost;
            staminaBar.SetStaminaUI(unit.stamina);
            unit.mana -= meleeAttack.GetComponent<AttackInfo>().manaCost;
            manaBar.SetManaUI(unit.mana);

            CancelInvoke("Stamina");
            InvokeRepeating("Stamina", unit.recoveryTimeStamina, unit.recoveryTimeStamina);
        }
        isMeleeKeyDown = false;
    }

    void RangedAttack ()
    {
        if (isRangedKeyDown && !isAttacking && !isRolling && unit.mana >= rangedAttack.GetComponent<AttackInfo>().manaCost)
        {
            if (rangedAttack.GetComponent<AttackInfo>().disableMovementWhileAttacking)
                MoveDirZero = true;

            isAttacking = true;
            isRangedAttacking = true;
            attackRotation = rotationPlayerToMouse;

            animator.speed = rangedAttack.GetComponent<AttackInfo>().attackSpeed;

            unit.stamina -= rangedAttack.GetComponent<AttackInfo>().staminaCost;
            staminaBar.SetStaminaUI(unit.stamina);
            unit.mana -= rangedAttack.GetComponent<AttackInfo>().manaCost;
            manaBar.SetManaUI(unit.mana);

            CancelInvoke("Mana");
            InvokeRepeating("Mana", unit.recoveryTimeMana, unit.recoveryTimeMana);
        }
        isRangedKeyDown = false;
    }

    #endregion

    #region Animation

    //Animating movement and rolling
    private void Animation()
    {
        /*
        string clipName;
        //int clipFrame;
        AnimatorClipInfo[] currentClipInfo;
        currentClipInfo = animator.GetCurrentAnimatorClipInfo(0);
        clipName = currentClipInfo[0].clip.name;
        */
        // actions in the same order as methods called in FixedUpdate
        if (isRolling)
        {
            if (rotationRollDir == 45f) animator.Play("Character_RollUpRight");
            else if (rotationRollDir == 90f) animator.Play("Character_RollUp");
            else if (rotationRollDir == 135f) animator.Play("Character_RollUpLeft");
            else if (rotationRollDir == 180f) animator.Play("Character_RollLeft");
            else if (rotationRollDir == 225f) animator.Play("Character_RollDownLeft");
            else if (rotationRollDir == 270f) animator.Play("Character_RollDown");
            else if (rotationRollDir == 315f) animator.Play("Character_RollDownRight");
            else animator.Play("Character_RollRight");
        }
        else if (isAttacking)
        {
            if (attackRotation > 45f && attackRotation <= 135f) animator.Play("Character_SlashUpRight");
            else if (attackRotation > 135f && attackRotation <= 225f) animator.Play("Character_SlashUpLeft");
            else if (attackRotation > 225f && attackRotation <= 315f) animator.Play("Character_SlashDownLeft");
            else animator.Play("Character_SlashDownRight");
        }
        // movement animation
        else if (moveDir != Vector3.zero)
        {
            if (rotationMoveDir == 45f) animator.Play("Character_UpRight");
            else if (rotationMoveDir == 90f) animator.Play("Character_Up");
            else if (rotationMoveDir == 135f) animator.Play("Character_UpLeft");
            else if (rotationMoveDir == 180f) animator.Play("Character_Left");
            else if (rotationMoveDir == 225f) animator.Play("Character_DownLeft");
            else if (rotationMoveDir == 270f) animator.Play("Character_Down");
            else if (rotationMoveDir == 315f) animator.Play("Character_DownRight");
            else animator.Play("Character_Right");
        }
        // first sprite of the movement animation if moveDir is zero
        else
        {
            if (rotationLastMoveDir == 45f) animator.Play("Character_UpRight", 0, 0);
            else if (rotationLastMoveDir == 90f) animator.Play("Character_Up", 0, 0);
            else if (rotationLastMoveDir == 135f) animator.Play("Character_UpLeft", 0, 0);
            else if (rotationLastMoveDir == 180f) animator.Play("Character_Left", 0, 0);
            else if (rotationLastMoveDir == 225f) animator.Play("Character_DownLeft", 0, 0);
            else if (rotationLastMoveDir == 270f) animator.Play("Character_Down", 0, 0);
            else if (rotationLastMoveDir == 315f) animator.Play("Character_DownRight", 0, 0);
            else animator.Play("Character_Right", 0, 0);
        }
    }

    #endregion

    #region audio

    // listening to events from animation
    public void StepAudio()
    {
        audioSource.PlayOneShot(step);
    }
    public void RollAudio()
    {
        audioSource.clip = roll;
        audioSource.Play();
    }
    #endregion

    #region Methods for animator
    public void SpawnAttack()
    {
        if (meleeAttack != null && isMeleeAttacking)
            Instantiate(meleeAttack, transform);

        if (rangedAttack != null && isRangedAttacking)
        {
            if (rangedAttack.GetComponent<AttackInfo>().hasTravelTime)
            {
                float _attackRotation = rotationPlayerToMouse;
                if (rotationPlayerToMouse > 0) _attackRotation = rotationPlayerToMouse -= 360f;
                Instantiate(rangedAttack, rb.position, Quaternion.Euler(0f, 0f, _attackRotation));
            }
            else
            {
                Instantiate(rangedAttack, worldPositionMouse, Quaternion.identity);
            }
        }
    }

    void ResetState()
    {
        isRolling = false;
        isAttacking = false;
        MoveDirZero = false;
        isMeleeAttacking = false;
        isRangedAttacking = false;
        animator.speed = 1f;
    }

    #endregion

    #region Movement and rotation

    Vector3 moveDirFromInput()
    {
        float moveX = 0f;
        float moveY = 0f;

        if (Input.GetKey(up)) moveY += 1f;
        if (Input.GetKey(down)) moveY -= 1f;
        if (Input.GetKey(left)) moveX -= 1f;
        if (Input.GetKey(right)) moveX += 1f;

        if (moveX == 0f && moveY == 0f || MoveDirZero)
        {
            return new Vector3(0, 0);
        }
        return new Vector3(moveX, moveY).normalized;
    }

    private void SetMouseWorldPossitionAndRotation()
    {
        worldPositionMouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        worldPositionMouse.z = 0f;
        distanceToMouse = worldPositionMouse - transform.position;

        Vector3 distanceToMouseNormalized = distanceToMouse.normalized;
        rotationPlayerToMouse = Mathf.Atan2(distanceToMouseNormalized.y, distanceToMouseNormalized.x) * Mathf.Rad2Deg;
        if (rotationPlayerToMouse < 0) rotationPlayerToMouse += 360f;

        rotationMoveDir = Mathf.Atan2(moveDir.y, moveDir.x) * Mathf.Rad2Deg;
        if (rotationMoveDir < 0) rotationMoveDir += 360f;

        rotationLastMoveDir = Mathf.Atan2(lastMoveDir.y, lastMoveDir.x) * Mathf.Rad2Deg;
        if (rotationLastMoveDir < 0) rotationLastMoveDir += 360f;

        rotationRollDir = Mathf.Atan2(rollDir.y, rollDir.x) * Mathf.Rad2Deg;
        if (rotationRollDir < 0) rotationRollDir += 360f;

    }

    #endregion
}
