﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{

    public int health = 10;
    public int maxHealth = 10;
    public float recoveryTimeHealth = 1f;

    public int stamina = 5;
    public int maxStamina = 10;
    public float recoveryTimeStamina = 0.5f;

    public int mana = 5;
    public int maxMana = 10;
    public float recoveryTimeMana = 1f;


    public bool agro = false;
    public string unitType = "Enemy";

    Rigidbody2D rb;
    HealthBar healthBar;
    ManaBar manaBar;
    StaminaBar staminaBar;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (gameObject.tag == "Player")
        {
            healthBar = FindObjectOfType<HealthBar>();
            manaBar = FindObjectOfType<ManaBar>();
            staminaBar = FindObjectOfType<StaminaBar>();
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (gameObject.tag == "Player")
        {
            if (collision.collider.CompareTag("Enemy") || collision.collider.CompareTag("WorldDamage"))
            {
                AttackInfo attackInfo = collision.collider.GetComponentInParent<AttackInfo>();

                Vector2 pushDir = (transform.position - attackInfo.transform.position).normalized;

                rb.AddForce(pushDir * attackInfo.pushForce, ForceMode2D.Impulse);

                health -= attackInfo.dmg;
                healthBar.SetHealthUI(health);
            }
        }

        if (gameObject.tag == "Enemy")
        {
            if (collision.collider.CompareTag("PlayerAttack") || collision.collider.CompareTag("WorldDamage"))
            {
                AttackInfo attackInfo = collision.collider.GetComponentInParent<AttackInfo>();
                Vector2 pushDir = (transform.position - attackInfo.transform.position).normalized;

                rb.AddForce(pushDir * attackInfo.pushForce, ForceMode2D.Impulse);

                health -= attackInfo.dmg;
                agro = true;
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(gameObject.tag == "Player")
        {
            if (collision.CompareTag("Enemy") || collision.CompareTag("WorldDamage"))
            {
                AttackInfo attackInfo = collision.GetComponentInParent<AttackInfo>();

                Vector2 pushDir = (transform.position - attackInfo.transform.position).normalized;

                rb.AddForce(pushDir * attackInfo.pushForce, ForceMode2D.Impulse);

                health -= attackInfo.dmg;
                healthBar.SetHealthUI(health);
            }
        }

        if(gameObject.tag == "Enemy")
        {
            if (collision.CompareTag("PlayerAttack") || collision.CompareTag("WorldDamage"))
            {
                AttackInfo attackInfo = collision.GetComponentInParent<AttackInfo>();
                Vector2 pushDir = (transform.position - attackInfo.transform.position).normalized;

                rb.AddForce(pushDir * attackInfo.pushForce, ForceMode2D.Impulse);

                health -= attackInfo.dmg;
                agro = true;
            }
        }

    }





}
