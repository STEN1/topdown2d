﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionRenderSorter : MonoBehaviour
{

    [SerializeField] bool runOnlyOnce = false;

    SpriteRenderer[] spriteRenderers;
    private void LateUpdate()
    {
        spriteRenderers = FindObjectsOfType<SpriteRenderer>();


        foreach (SpriteRenderer spriteRenderer  in spriteRenderers)
        {
            
            spriteRenderer.sortingOrder = (int)(spriteRenderer.transform.position.y * -100);
        }
        if (runOnlyOnce) enabled = false;

    }
}
