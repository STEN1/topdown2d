﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class IceShatter : MonoBehaviour
{
    CircleCollider2D circleCollider2D;
    Light2D light2D;
    float t = 0f;

    private void Start()
    {
        light2D = GetComponentInChildren<Light2D>();
        circleCollider2D = GetComponent<CircleCollider2D>();
    }

    private void FixedUpdate()
    {
        light2D.pointLightOuterRadius = Mathf.Lerp(10f, 0f, t);

        t += Time.fixedDeltaTime;
    }
    public void Destroy()
    {
        Destroy(gameObject);
    }

    public void DisableCollider()
    {
        circleCollider2D.enabled = false;
    }
}
