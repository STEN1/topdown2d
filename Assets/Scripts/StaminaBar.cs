﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour
{
    public Slider slider;

    public void SetStaminaUI (int stamina, int maxStamina)
    {
        slider.maxValue = maxStamina;
        slider.value = stamina;
    }

    public void SetStaminaUI(int stamina)
    {
        slider.value = stamina;
    }
}
