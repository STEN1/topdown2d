﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class EnemyGhostGFX : MonoBehaviour
{
    bool up = true;

    public float bobAmount = 1f;
    public float bobSpeed = 1f;

    public float maxLight = 1f;
    public float minLight = 0.5f;
    public float LightCycleTime = 0.5f;


    Light2D light2D;
    float t = 0f;
    private void Start()
    {
        light2D = GetComponentInChildren<Light2D>();
    }

    private void FixedUpdate()
    {

        //ghost bobbing
        if (up)
        {
            transform.localPosition += new Vector3(0f, bobSpeed * Time.fixedDeltaTime, 0f);

            if (transform.localPosition.y > bobAmount)
            {
                up = false;
            }
        }

        if (!up)
        {
            transform.localPosition += new Vector3(0f, -bobSpeed * Time.fixedDeltaTime, 0f);

            if (transform.localPosition.y < -bobAmount)
            {
                up = true;
            }
        }

        //ghost light
        light2D.intensity = Mathf.Lerp(minLight, maxLight, t);


        t += LightCycleTime * Time.deltaTime;


        if (t > 1.0f)
        {
            float temp = maxLight;
            maxLight = minLight;
            minLight = temp;
            t = 0.0f;
        }


    }
    public void Death()
    {
        Destroy(transform.parent.gameObject);
    }
}
