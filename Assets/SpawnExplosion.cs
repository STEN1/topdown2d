﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnExplosion : MonoBehaviour
{
    public GameObject shatterEffect;
    public void Explosion()
    {
        AttackInfo attackInfo = GetComponentInParent<AttackInfo>();
        AttackInfo attackInfoShatterEffect = shatterEffect.GetComponent<AttackInfo>();
        attackInfoShatterEffect.dmg = attackInfo.dmg;
        attackInfoShatterEffect.pushForce = attackInfo.pushForce;
        attackInfoShatterEffect.dmgType = attackInfo.dmgType;
        Instantiate(shatterEffect, transform.position, Quaternion.identity);
    }
}
