﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorSpike : MonoBehaviour
{
    PolygonCollider2D polygonCollider;
    private void Awake()
    {
        polygonCollider = GetComponent<PolygonCollider2D>();
    }
    public void EnableSpike()
    {
        polygonCollider.enabled = true;
    }

    public void DisableSpike()
    {
        polygonCollider.enabled = false;
    }
}
