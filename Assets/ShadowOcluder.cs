﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class ShadowOcluder : MonoBehaviour
{
    ShadowCaster2D shadowCaster2D;
    private void Start()
    {
        shadowCaster2D = GetComponent<ShadowCaster2D>();
        shadowCaster2D.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("MainCamera"))
        {

            shadowCaster2D.enabled = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCamera"))
        {
            shadowCaster2D.enabled = false;
        }
    }
}
