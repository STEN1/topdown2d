﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAttack : MonoBehaviour
{
    Character character;
    Animator animator;
    GameObject spark;

    public GameObject colliderUpRight;
    public GameObject colliderUpLeft;
    public GameObject colliderDownRight;
    public GameObject colliderDownLeft;


    private void Start()
    {
        animator = GetComponent<Animator>();
        character = GetComponentInParent<Character>();
        spark = Resources.Load("Effects/Spark/Spark", typeof(GameObject)) as GameObject;



        if (character.attackRotation > 45f && character.attackRotation <= 135f) animator.Play("Sword_UpRight");
        else if (character.attackRotation > 135f && character.attackRotation <= 225f) animator.Play("Sword_UpLeft");
        else if (character.attackRotation > 225f && character.attackRotation <= 315f) animator.Play("Sword_DownLeft");
        else animator.Play("Sword_DownRight");
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        { 
            Vector2 hitPos = collision.transform.position;
            Instantiate(spark, hitPos, Quaternion.identity);
        }
    }


    #region Methods for animator
    public void EnableColliderAttackUpRight ()
    {
        colliderUpRight.SetActive(true);
    }
    public void EnableColliderAttackUpLeft()
    {
        colliderUpLeft.SetActive(true);
    }
    public void EnableColliderAttackDownRight()
    {
        colliderDownRight.SetActive(true);
    }
    public void EnableColliderAttackDownLeft()
    {
        colliderDownLeft.SetActive(true);
    }

    public void DisableColliderAttackUpRight()
    {
        colliderUpRight.SetActive(false);
    }
    public void DisableColliderAttackUpLeft()
    {
        colliderUpLeft.SetActive(false);
    }
    public void DisableColliderAttackDownRight()
    {
        colliderDownRight.SetActive(false);
    }
    public void DisableColliderAttackDownLeft()
    {
        colliderDownLeft.SetActive(false);
    }


    public void AttackEnd()
    {
        Destroy(gameObject);
    }


    #endregion
}
